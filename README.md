primal-client: a Java Client for the Primal REST API
====================================================

Copyright InsightNG (http://www.insightng.com/) (c) 2012-2013. See LICENSE.txt for details.

Developer instructions
----------------------

This project uses Apache Maven (http://maven.apache.org/), and we recommend you
use Eclipse. To set up your development environment perform the following
steps:

1. Get a local copy of the git repository (`git clone ...`)
1. Install Eclipse (http://www.eclipse.org/)
1. Install EGit, the Eclipse plugin for Git (http://www.eclipse.org/egit/)
1. In your local git clone, perform `mvn eclipse:eclipse` to generate Eclipse IDE settings
1. In Eclipse, select 'Import' -> 'General' -> 'Existing project into Workspace' and select your local git repo for the project location. Make sure 'Copy projects into workspace' is *not* checked.
1. In the Eclipse package explorer, right-click on your newly checked out project, select 'Team' -> 'Share project', and select 'Git'. Click 'Next' and in the next screen click 'Finish'. Your Eclipse Git plugin will now be linked with your git repo, so you can commit, push, pull, etc directly from Eclipse.
