/* Copyright InsightNG (http://www.insightng.com/) 2012-2013
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   - Neither the name of InsightNG nor the names of its contributors may be used 
 *     to endorse or promote products derived from this software without specific 
 *     prior written permission.
 *     
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 *  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.insightng.thirdparty.primal;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.OutputStream;
import java.util.Properties;

import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.vocabulary.DC;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.model.vocabulary.RDFS;
import org.eclipse.rdf4j.model.vocabulary.SKOS;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFHandler;
import org.eclipse.rdf4j.rio.RDFHandlerException;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.rio.helpers.BufferedGroupingRDFHandler;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.insightng.thirdparty.primal.vocabulary.PRIMAL;

/**
 * Tests PrimalClient by making live calls to Primal.
 */
public class TestPrimalClient {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	private static final PrimalClient client;

	private String testTopic = "SPARQL/java;tools";

	private static final String testUser = "unit-test-user";

	static {
		PrimalAccount account = null;
		try {
			account = getPrimalAccount();
		} catch (Exception e) {
			e.printStackTrace();
		}
		client = new PrimalClient(account, getTestUser());
	}

	private static String getTestUser() {
		return testUser;
	}

	@Test
	@Ignore
	public void testHangingGet() throws Exception {
		//client.addConcept(testTopic, true);

		long start = System.currentTimeMillis();
		Model model = client.getResult(testTopic, 0, 0, 30);
		long end = System.currentTimeMillis();

		double durationSeconds = (end - start) / 1000.0;
		logger.debug("took " + durationSeconds + " seconds");

		// add 5 seconds for latency.
		assertTrue("took " + durationSeconds + " seconds", durationSeconds <= 35.0);

		assertNotNull(model);

		Model responseInfo = model.filter(null, null, null, PRIMAL.RESPONSE_INFO);

		assertTrue(responseInfo.contains(null, PRIMAL.PRIMAL_SUBSTITUTE_USED, null));
		assertTrue(responseInfo.contains(null, PRIMAL.UNIQUE_TERM_COUNT, null));

		writeModel(model, RDFFormat.TRIG, System.out);

		// writeModel(model, RDFFormat.TRIG, new FileOutputStream(new
		// File("/Users/jeen/test-primal.trig")));

		// client.deleteConcept(testTopic);
	}

	private void writeModel(Model model, RDFFormat format, OutputStream out) throws RDFHandlerException {

		final RDFHandler writer = new BufferedGroupingRDFHandler(Rio.createWriter(format, out));
		writer.startRDF();
		writer.handleNamespace("rdf", RDF.NAMESPACE);
		writer.handleNamespace("rdfs", RDFS.NAMESPACE);
		writer.handleNamespace("skos", SKOS.NAMESPACE);
		writer.handleNamespace("dc", DC.NAMESPACE);
		writer.handleNamespace("primal", PRIMAL.NAMESPACE);

		for (final Statement st : model) {
			writer.handleStatement(st);
		}
		writer.endRDF();
	}

	private static PrimalAccount getPrimalAccount() throws Exception {
		// Read properties file.
		final Properties properties = new Properties();
		properties.load(TestPrimalClient.class.getResourceAsStream("/config.properties.example"));

		final String username = properties.getProperty("primal.user");
		if (username == null) {
			throw new Exception("missing value for primal.user");
		}
		final String password = properties.getProperty("primal.key");
		if (password == null) {
			throw new Exception("missing value for primal.key");
		}

		final String appId = properties.getProperty("primal.app.id");
		if (appId == null) {
			throw new Exception("missing value for primal.app.id");
		}

		final String appKey = properties.getProperty("primal.app.key");
		if (appKey == null) {
			throw new Exception("missing value for primal.app.key");
		}

		PrimalAccount account = new PrimalAccount(username, password, appId, appKey);
		return account;
	}
}
