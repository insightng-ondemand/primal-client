package com.insightng.thirdparty.primal;

import static java.net.HttpURLConnection.HTTP_OK;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.HashSet;
import java.util.Set;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.AbstractHttpEntity;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.message.BasicHttpResponse;
import org.apache.http.protocol.HttpContext;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.model.vocabulary.DC;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.insightng.thirdparty.primal.vocabulary.PRIMAL;

public class PrimalClientTest {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	private PrimalAccount primalAccount;
	private HttpClient httpClient;
	private PrimalClient primalClient;

	@Before
	public void before() {
		primalAccount = new PrimalAccount("insightngtest", "testpassword", "testappkey", "testappid");
		httpClient = mock(HttpClient.class);
		primalClient = new PrimalClient(primalAccount, httpClient);
	}

	@After
	public void after() {
		primalAccount = null;
		httpClient = null;
		primalClient = null;
	}

	@Test
	public void testCreateBody() throws Exception {
		String topic = "\"pop star\"/symbol (Prince);“David Bowie”";
		String expected = "{\"uris\":[\"\\\"pop star\\\"/symbol (Prince);“David Bowie”\"]}";
		String actual = primalClient.createQueryBody(topic);
		assertEquals(expected, actual);

		topic = "shelter/\"caves (outdoor)\";house building;tents";
		expected = "{\"uris\":[\"shelter/\\\"caves (outdoor)\\\";house building;tents\"]}";
		actual = primalClient.createQueryBody(topic);
		assertEquals(expected, actual);
	}

	@Test
	public void testParseResponse() throws Exception {
		// json response used with /recommendations query for: /NASA/Neil%20Armstrong
		AbstractHttpEntity entity = new InputStreamEntity(getClass().getResourceAsStream("neilarmstrong.json"));
		HttpResponse response = new BasicHttpResponse(HttpVersion.HTTP_1_1, HTTP_OK, null);
		response.setEntity(entity);
		when(httpClient.execute(any(HttpUriRequest.class), any(HttpContext.class))).thenReturn(response);

		Model model = primalClient.getResult("topic", "contentSource", 0.6f, 20, 0);
		assertEquals(5, model.filter(null, RDF.TYPE, PRIMAL.CONTENT_ITEM).subjects().size());
		Set<String> refs = new HashSet<String>();
		for (Resource item : model.filter(null, RDF.TYPE, PRIMAL.CONTENT_ITEM).subjects()) {
			final Literal directReference = Models.objectLiteral(model.filter(item, DC.RELATION, null)).orElse(null);
			Literal name = Models.objectLiteral(model.filter(item, DC.TITLE, null)).orElse(null);
			assertNotNull(directReference);
			assertTrue(refs.add(directReference.stringValue()));
			assertNotNull(name);
			logger.debug(name.stringValue());
		}
	}
}
