package com.insightng.thirdparty.primal;

import static org.junit.Assert.assertEquals;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.LinkedHashModel;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.insightng.thirdparty.primal.vocabulary.ResponseKey;

public class ResponseValuesTest {

	private final ValueFactory vf = SimpleValueFactory.getInstance();
	private final IRI subject = vf.createIRI("http://example.org/stub/subject");
	private final IRI property = vf.createIRI("http://example.org/stub/property");
	private final IRI context = vf.createIRI("http://example.org/stub/context");
	
	private Model model; 
	
	@Before
	public void setUp() throws Exception {
		model = new LinkedHashModel();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testProcessDateValue() {
		String dateString = "August 25, 2011 1:16:34 PM EST";
		
		Calendar c = Calendar.getInstance(TimeZone.getTimeZone("EST"));
		c.clear();
		c.set(2011, 7, 25, 13, 16, 34);
		
		testDateValue(dateString, c.getTime());
	}

	private void testDateValue(String dateString, Date expected) {
		JsonObject jsonObject = new JsonObject();
		jsonObject.add(ResponseKey.SCHEMA_DATE_PUBLISHED, new JsonPrimitive(dateString));
		Date date = ResponseValues.processDateValue(subject, property, model, context, jsonObject, ResponseKey.SCHEMA_DATE_PUBLISHED);
		
		
		assertEquals(expected, date);
	}
}
