package com.insightng.thirdparty.primal.response;

import org.eclipse.rdf4j.model.Model;

public class PrimalResponse {

	private final Model model;

	public PrimalResponse(Model model) {
		this.model = model;
	}
	
	public Model getModel() {
		return model;
	}
	
	public ResponseInfo getResponseInfo() {
		return new ResponseInfo(this);
	}
	
}
