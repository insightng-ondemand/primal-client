package com.insightng.thirdparty.primal.response;

import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.IRI;

import com.insightng.thirdparty.primal.vocabulary.PRIMAL;

public class ResponseInfo {

	private final PrimalResponse response;

	ResponseInfo(PrimalResponse response) {
		this.response = response;
	}

	public int getContentCount() {
		return getLiteralValue(PRIMAL.UNIQUE_TERM_COUNT).intValue();
	}

	public int getTotalConceptsCount() {
		return getLiteralValue(PRIMAL.TOTAL_CONTEXT_TERM_COUNT).intValue();
	}

	public int getConceptCount() {
		return getLiteralValue(PRIMAL.PRIMAL_SUBSTITUTE_USED).intValue();
	}

	private Literal getLiteralValue(IRI prop) {
		return this.response.getModel().filter(null, prop, null, PRIMAL.RESPONSE_INFO).objectLiteral().orElse(null);
	}
}
