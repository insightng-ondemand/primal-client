package com.insightng.thirdparty.primal;

/**
 * Primal account data needed for authenticating with Primal services.
 * 
 * @author Jeen Broekstra
 * 
 */
public class PrimalAccount {

	private final String username;
	private final String password;
	private final String applicationID;
	private final String applicationKey;

	public PrimalAccount(String username, String password, String applicationID, String applicationKey) {
		this.username = username;
		this.password = password;
		this.applicationID = applicationID;
		this.applicationKey = applicationKey;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public String getApplicationID() {
		return applicationID;
	}

	public String getApplicationKey() {
		return applicationKey;
	}
}
