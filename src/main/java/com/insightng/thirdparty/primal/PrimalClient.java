/*
 * Copyright InsightNG (http://www.insightng.com/) 2012-2013
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * - Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * - Neither the name of InsightNG nor the names of its contributors may be used
 * to endorse or promote products derived from this software without specific
 * prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.insightng.thirdparty.primal;

import static java.net.HttpURLConnection.HTTP_CREATED;
import static java.net.HttpURLConnection.HTTP_OK;
import static java.net.HttpURLConnection.HTTP_UNAUTHORIZED;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.AuthenticationException;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.protocol.HttpCoreContext;
import org.apache.http.util.EntityUtils;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.LinkedHashModel;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.vocabulary.DC;
import org.eclipse.rdf4j.model.vocabulary.FOAF;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.model.vocabulary.SKOS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.google.gson.stream.JsonReader;
import com.insightng.thirdparty.primal.exception.PrimalAuthenticationException;
import com.insightng.thirdparty.primal.exception.PrimalException;
import com.insightng.thirdparty.primal.vocabulary.PRIMAL;
import com.insightng.thirdparty.primal.vocabulary.PrimalTimePeriod;
import com.insightng.thirdparty.primal.vocabulary.ResponseKey;

/**
 * PrimalClient provides full client query and manipulation access to the interest graphs of a specified user
 * on the Primal server. The Primal JSON results are processed and converted to RDF models (using the Sesame
 * RDF API).
 * <p>
 * Updated June 2017 for Primal API v2:
 * <ul>
 * <li>Change to <code>/recommendations</code> endpoint</li>
 * <li>Sub-accounts do not log in, the main account always logs in and the current sub-account is flagged as an IRI parameter <code>?uid=</code></li>
 * <li>Sub-account user management is removed - just use <code>?uid=</code> and Primal auto-creates sub accounts on the fly</li>
 * <li>Implemented limited types of content to include</li>
 * <li>NB: Adding/removing concepts/collections/sources has changed, but the changes are not implemented in this class yet</li>
 * </ul>
 * 
 * @see <a href="https://corp.primal.com/developers/">Primal Developers</a>
 * @see <a href="http://www.rdf4j.org/">RDF4J</a>
 * @author Jeen Broekstra
 * @author Douglas Campbell
 */
public class PrimalClient {

	/* public constants */

	/**
	 * Maximum timeout for establishing a connection and for keeping it open, in seconds.
	 */
	public static final int MAX_WAIT_TIME = 30;

	/**
	 * The Primal data API URL (<code>https://api.primal.com/v2</code> ).
	 */
	public static final String PRIMAL_API_URL = "https://api.primal.com/v2/recommendations/";
	public static final String PRIMAL_API_SUBACCOUNT_STEM = "insightng!";
	public static final String PRIMAL_API_SUBACCOUNT_DEFAULT_USER = "defaultUser";
	public static final int PRIMAL_API_TOPIC_MAX_LENGTH = 255;

	public static final String PRIMAL_API_HEADER_APP_ID  = "Primal-App-ID";
	public static final String PRIMAL_API_HEADER_APP_KEY  = "Primal-App-Key";
	public static final String PRIMAL_API_PARAM_USERNAME  = "uid";
	public static final String PRIMAL_API_PARAM_MAX_CONTENT_ITEMS  = "maxContentItems";
	public static final String PRIMAL_API_PARAM_MIN_CONTENT_SCORE  = "minContentScore";
	public static final String PRIMAL_API_PARAM_TYPES  = "types";
	public static final String PRIMAL_API_PARAM_CONTENT_SOURCES  = "contentSources";
	public static final String PRIMAL_API_PARAM_CONTENT_COLLECTIONS  = "contentCollections";

	// excluded: ImageObject, MobileApplication, Product, ProfilePage, SoftwareApplication
	public static final String PRIMAL_API_DEFAULT_TYPES = "BlogPosting,Book,MedicalScholarlyArticle,NewsArticle,SocialMediaPosting,VideoObject,WebPage";
	
	/* private fields */

	private final PrimalAccount primalAccount;

	private static final Logger logger = LoggerFactory.getLogger(PrimalClient.class);

	private final HttpClient httpClient;

	private static final URL primalApi;

	private static final ValueFactory vf = SimpleValueFactory.getInstance();

	private static final PoolingHttpClientConnectionManager cm;

    private final UsernamePasswordCredentials accountCredentials;
    private String subAccount;

    static {
		try {
			primalApi = new URL(PRIMAL_API_URL);
		} catch (final MalformedURLException e) {
			throw new RuntimeException(e);
		}

		cm = new PoolingHttpClientConnectionManager();
		cm.setMaxTotal(100);
		cm.setDefaultMaxPerRoute(20);

		Runtime.getRuntime().addShutdownHook(new Thread("PrimalClient-shutdown") {
			public void run() {
				cm.close();
				cm.shutdown();
			}
		});
	}

	/* constructors */

	/**
	 * Creates a new Primal Client with the supplied {@link PrimalAccount} credentials
	 */
	public PrimalClient(PrimalAccount primalAccount) {
		this(primalAccount, (String) null);
	}

    /**
     * Creates a new Primal Client with the supplied {@link PrimalAccount} credentials and the supplied
     * sub-account credentials.
     * 
     * @param primalAccount
     *            primal account data containing main account credentials and app key and identifier.
     * @param subAccountId
     *            the user ID of the Primal user sub-account (a standard insightng prefix is added
     *            for use in Primal)
     */
    public PrimalClient(PrimalAccount primalAccount, String subAccountId) {
        this.primalAccount = primalAccount;
        this.subAccount = buildCompositeSubAccountUsername(subAccountId);
        if (primalAccount != null) {
            this.accountCredentials = new UsernamePasswordCredentials(primalAccount.getUsername(), primalAccount.getPassword());
        } else {
            this.accountCredentials = null;
        }
		this.httpClient = createHttpClient(primalAccount);
    }
    
	/**
	 * constructor for testing purposes only.
	 * 
	 * @param primalAccount
	 * @param httpClient
	 */
	PrimalClient(PrimalAccount primalAccount, HttpClient httpClient) {
		this.primalAccount = primalAccount;
        if (primalAccount != null) {
            this.accountCredentials = new UsernamePasswordCredentials(primalAccount.getUsername(), primalAccount.getPassword());
        } else {
            this.accountCredentials = null;
        }
		this.httpClient = httpClient;
	}
	
	public String getSubAccountId() {
		return subAccount;
	}
	
	/**
	 * Adds a standard prefix PRIMAL_API_SUBACCOUNT_STEM to the supplied
	 * InsightNG user ID for use as a sub account in Primal. Note that the
	 * standard prefix has changed over time.
	 * 
	 * @param subAccountUserId
	 *            the ID number for the user account (not the username which may
	 *            change)
	 */
	public static String buildCompositeSubAccountUsername(String subAccountUserId) {
        if (subAccountUserId == null || subAccountUserId.trim().length() < 1) {
            return PRIMAL_API_SUBACCOUNT_STEM + PRIMAL_API_SUBACCOUNT_DEFAULT_USER;
        } else {
            return PRIMAL_API_SUBACCOUNT_STEM + subAccountUserId;
        }
	}

	private static HttpClient createHttpClient(PrimalAccount primalAccount) {
		final RequestConfig reqConfig = RequestConfig.custom().setSocketTimeout((MAX_WAIT_TIME + 2) * 1000)
				.setConnectionRequestTimeout((MAX_WAIT_TIME + 2) * 1000)
				.setConnectTimeout((MAX_WAIT_TIME + 2) * 1000).build();
		CredentialsProvider credsProvider = new BasicCredentialsProvider();
		credsProvider.setCredentials(new AuthScope(primalApi.getHost(), AuthScope.ANY_PORT),
				new UsernamePasswordCredentials(primalAccount.getUsername(), primalAccount.getPassword()));

		return HttpClients.custom().setConnectionManager(cm).setDefaultRequestConfig(reqConfig)
				.setDefaultCredentialsProvider(credsProvider).build();
	}

	/* public methods */

	/**
	 * Creates a new content source.
	 * 
	 * @param contentSourceName
	 *            the name of the new content source
	 * @param defaultContentSource
	 *            flag to indicate if this content source should be considered the default source to operate
	 *            on
	 * @param contents
	 *            specifies which content to include in this source, and optionally for each entry an
	 *            instruction to only consider content published in that time period
	 * @throws PrimalException
	 *             if the content source could not be created.
	 */
	public void createNewContentSource(String contentSourceName, boolean defaultContentSource,
			Map<IRI, PrimalTimePeriod> contents) throws PrimalException {

		JsonObject newContentSource = new JsonObject();
		newContentSource.add("default", new JsonPrimitive(defaultContentSource));

		JsonObject contentsObject = new JsonObject();
		for (Entry<IRI, PrimalTimePeriod> entry : contents.entrySet()) {

			JsonObject timePeriodValue = new JsonObject();
			if (entry.getValue() != null) {
				timePeriodValue.add("time", new JsonPrimitive(entry.getValue().toString()));
			}

			contentsObject.add(entry.getKey().stringValue(), timePeriodValue);
		}

		newContentSource.add("contents", contentsObject);

		JsonObject payload = new JsonObject();
		payload.add(contentSourceName, newContentSource);

		HttpPost method = new HttpPost(PRIMAL_API_URL + "/@Sources");

		try {
			configureRequest(method, null);

			StringEntity requestEntity = new StringEntity(new Gson().toJson(payload),
					ContentType.APPLICATION_JSON);

			method.setEntity(requestEntity);

			final HttpCoreContext context = new HttpCoreContext();
			final HttpResponse response = httpClient.execute(method, context);
			logRequestDetails(method, context);

			try {
				final int httpCode = response.getStatusLine().getStatusCode();
				if (HTTP_CREATED != httpCode) {
					if (HTTP_UNAUTHORIZED == httpCode) {
						throw new PrimalAuthenticationException();
					} else {
						throw new PrimalException("error executing content source creation request",
								httpCode);
					}
				}
			} finally {
				EntityUtils.consumeQuietly(response.getEntity());
			}
		} catch (final IOException e) {
			logger.warn("error executing content source creation request", e);
			throw new PrimalException("error executing content source creation request", 0, e);
		}
	}

	public List<IRI> getCustomContentSources() throws PrimalException {

		final String requestURL = PRIMAL_API_URL + "@Sources";

		final HttpGet method = new HttpGet(requestURL);
		try {
			configureRequest(method, null);

			final HttpCoreContext context = new HttpCoreContext();
			final HttpResponse response = httpClient.execute(method, context);
			logRequestDetails(method, context);

			try {
				final int httpCode = response.getStatusLine().getStatusCode();
				if (httpCode == HTTP_OK) {

					final JsonReader reader = new JsonReader(
							new InputStreamReader(response.getEntity().getContent()));

					final JsonParser jsonParser = new JsonParser();
					final JsonObject responseObject = jsonParser.parse(reader).getAsJsonObject();

					List<IRI> sources = new ArrayList<IRI>();
					for (Entry<String, JsonElement> entry : responseObject.entrySet()) {
						sources.add(vf.createIRI(entry.getKey()));
					}
					return sources;
				} else if (HTTP_UNAUTHORIZED == httpCode) {
					logResponseDetails(response);
					throw new PrimalAuthenticationException();
				} else {
					logResponseDetails(response);
					throw new PrimalException("error retrieving content sources", httpCode);
				}
			} finally {
				EntityUtils.consumeQuietly(response.getEntity());
			}
		} catch (final IOException e) {
			logger.warn("error retrieving content sources", e);
			throw new PrimalException("error retrieving content sources", 0, e);
		}
	}

	public Model getResult(String topic) throws PrimalException {
		return getResult(topic, null);
	}

	/**
	 * Returns an RDF model describing the supplied interest network. The model will contain concepts that
	 * have been explicitly provided by the user, as well as synthesized concepts that Primal has generated
	 * and inserted into the interest network. The concepts within the response model will be validated with
	 * content taken from the website(s) defined within {source}.
	 * 
	 * @param topic
	 *            Specification of the core topic of interest for this search. Values should be provided in a
	 *            forward slash and/or semi-colon delimited format, where slashes delimit a conceptual
	 *            hierarchy, and semi-colons provide a breadth of conceptual detail at the same hierarchical
	 *            level. For example:
	 *            <ul>
	 *            <li><code>tidal research</code><br>
	 *            Implies a simple use case of interest in the field of tidal research.</li>
	 *            <li><code>baking/cookies</code><br>
	 *            Implies interest in cookies of the edible variety rather than the browser data storage
	 *            variety.</li>
	 *            <li><code>shelter/caves;houses;tents</code><br>
	 *            Implies interest in the concepts of caves, houses and tents as being types of shelters.</li>
	 *            </ul>
	 * @param contentSource
	 *            source of content.
	 * 
	 * @return a {@link Model} containing RDF statements representing a description of the interest network.
	 *         Concepts are represented as instances of the class <code>skos:Concept</code> 
	 *         having a <code>skos:prefLabel</code> property
	 *         with their human-readable label. Content is represented as instances of the class
	 *         <code>primal:ContentItem</code>.
	 * @throws PrimalException
	 *             if an error occurred in communicating with the server.
	 */
	public Model getResult(String topic, String contentSource) throws PrimalException {
		return getResult(topic, contentSource, 0, 0, 0);
	}

	public Model getResult(String topic, int wait) throws PrimalException {
		return getResult(topic, null, wait);
	}

	/**
	 * Returns an RDF model describing the supplied interest network. The model will contain concepts that
	 * have been explicitly provided by the user, as well as synthesized concepts that Primal has generated
	 * and inserted into the interest network. The concepts within the response model will be validated with
	 * content taken from the website(s) defined within {source}.
	 * 
	 * @param topic
	 *            Specification of the core topic of interest for this search. Values should be provided in a
	 *            forward slash and/or semi-colon delimited format, where slashes delimit a conceptual
	 *            hierarchy, and semi-colons provide a breadth of conceptual detail at the same hierarchical
	 *            level. For example:
	 *            <ul>
	 *            <li><code>tidal research</code><br>
	 *            Implies a simple use case of interest in the field of tidal research.</li>
	 *            <li><code>baking/cookies</code><br>
	 *            Implies interest in cookies of the edible variety rather than the browser data storage
	 *            variety.</li>
	 *            <li><code>shelter/caves;houses;tents</code><br>
	 *            Implies interest in the concepts of caves, houses and tents as being types of shelters.</li>
	 *            </ul>
	 * @param contentSource
	 *            source of content.
	 * @param wait
	 *            Instruct Primal to wait for a specified period of time (in seconds) until a request reaches
	 *            a specified state before returning a response. Can only be used in conjunction with a
	 *            specific status parameter.
	 * 
	 * @return a {@link Model} containing RDF statements representing a description of the interest network.
	 *         Concepts are represented as instances of the class <code>skos:Concept</code> 
	 *         having a <code>skos:prefLabel</code> property
	 *         with their human-readable label. Content is represented as instances of the class
	 *         <code>primal:ContentItem</code>.
	 * @throws PrimalException
	 *             if an error occurred in communicating with the server.
	 */
	public Model getResult(String topic, String contentSource, int wait) throws PrimalException {
		return getResult(topic, contentSource, 0, 0, wait);
	}

	public Model getResult(String topic, float minScore, int maxContentCount, int wait)
			throws PrimalException {
		return getResult(topic, null, minScore, maxContentCount, wait);
	}

	/**
	 * Returns an RDF model describing the supplied interest network. The model will contain concepts that
	 * have been explicitly provided by the user, as well as synthesized concepts that Primal has generated
	 * and inserted into the interest network. The concepts within the response model will be validated with
	 * content taken from the website(s) defined within {source}.
	 * 
	 * @param topic
	 *            Specification of the core topic of interest for this search. Values should be provided in a
	 *            forward slash and/or semi-colon delimited format, where slashes delimit a conceptual
	 *            hierarchy, and semi-colons provide a breadth of conceptual detail at the same hierarchical
	 *            level. All values should be URI encoded. For example:
	 *            <ul>
	 *            <li><code>tidal research</code><br>
	 *            Implies a simple use case of interest in the field of tidal research.</li>
	 *            <li><code>baking/cookies</code><br>
	 *            Implies interest in cookies of the edible variety rather than the browser data storage
	 *            variety.</li>
	 *            <li><code>shelter/caves;houses;outdoor%20tents</code><br>
	 *            Implies interest in the concepts of caves, houses and outdoor tents as being types of shelters.</li>
	 *            </ul>
	 * @param contentSource
	 *            source of content.
	 * @param minScore
	 *            modifies the results returned based on relevancy by changing the minimum acceptable
	 *            primal:HasScore value. If set to zero, all results will be returned.
	 * @param maxContentCount
	 *            modifies the number of results returned by changing the maximum number of content items to
	 *            return. For example, you would use this parameter to return a "top ten" list of content
	 *            items. If set to zero, all result content items will be returned.
	 * @param wait
	 *            Instruct Primal to wait for a specified period of time (in seconds) until a request reaches
	 *            a specified state before returning a response. Can only be used in conjunction with a
	 *            specific status parameter.
	 * 
	 * @return a {@link Model} containing RDF statements representing a description of the interest network.
	 *         Concepts are represented as instances of the class <code>skos:Concept</code> 
	 *         having a <code>skos:prefLabel</code> property
	 *         with their human-readable label. Content is represented as instances of the class
	 *         <code>primal:ContentItem</code>.
	 * @throws PrimalException
	 *             if an error occurred in communicating with the server.
	 */
	public Model getResult(String topic, String contentSource, float minScore, int maxContentCount, int wait)
			throws PrimalException {

		if (wait > MAX_WAIT_TIME) {
			throw new PrimalException("wait time is not allowed to exceed " + MAX_WAIT_TIME + " seconds", 0);
		}
		if (topic == null) {
			throw new PrimalException("topic is empty", 0);
		}

		// prepare topic
		if (!topic.startsWith("/")) {
			topic = "/" + topic;
		}
		topic = topic.replace(" ", "%20");

		final Model model = new LinkedHashModel();

		
		final java.net.URI requestURL = createRequestURI(contentSource, minScore, maxContentCount,
				wait);

		final HttpPost method = new HttpPost(requestURL);
		try {
			configureRequest(method, topic);
			final HttpCoreContext context = new HttpCoreContext();
			logRequestDetails(method, context);
			final HttpResponse response = httpClient.execute(method, context);
			try {
				final int httpCode = response.getStatusLine().getStatusCode();
				logger.debug("response code: " + httpCode);
				if (httpCode == HTTP_OK) {
					final JsonReader reader = new JsonReader(
							new InputStreamReader(response.getEntity().getContent()));

					final JsonParser jsonParser = new JsonParser();
					final JsonObject responseObject = jsonParser.parse(reader).getAsJsonObject();

					//logger.trace("json response string: " + responseObject.toString());

					final JsonObject responseInfo = responseObject
							.getAsJsonObject(ResponseKey.PRIMAL_RESPONSE_INFO);

					
					if (responseInfo != null) {
						logger.debug("processing primal response info in response");

						final Resource responseInfoId = vf.createIRI(requestURL.toString());

						ResponseValues.processIntValue(responseInfoId, PRIMAL.UNIQUE_TERM_COUNT, model,
								PRIMAL.RESPONSE_INFO, responseInfo, ResponseKey.PRIMAL_UNIQUE_TERM_COUNT);

						ResponseValues.processIntValue(responseInfoId, PRIMAL.TOTAL_CONTEXT_TERM_COUNT, model,
								PRIMAL.RESPONSE_INFO, responseInfo, ResponseKey.PRIMAL_CONTEXT_TERM_COUNT);

						ResponseValues.processBooleanValue(responseInfoId, PRIMAL.PRIMAL_SUBSTITUTE_USED, model,
								PRIMAL.RESPONSE_INFO, responseInfo, ResponseKey.PRIMAL_SUBSTITUTE_USED);
					}

					
					logger.debug("processing main response body...");
					final JsonElement dcCollection = responseObject.get(ResponseKey.RDF_GRAPH);

					if (dcCollection.isJsonArray()) {
						final JsonArray array = dcCollection.getAsJsonArray();

						int countContent = 0;
						int countConcept = 0;
						int countUnknown = 0;

						for (int i = 0; i < array.size(); i++) {
							final JsonObject contentItem = array.get(i).getAsJsonObject();

							String rdfIdentifier = contentItem.get(ResponseKey.RDF_IDENTIFIER)
									.getAsString();
							// content id
							rdfIdentifier = rdfIdentifier.replace("?u=", "");
							rdfIdentifier = URLDecoder.decode(rdfIdentifier, "UTF8");
							// context id
							if (!rdfIdentifier.startsWith("http")) {
								rdfIdentifier = PRIMAL.TOPIC_NAMESPACE + rdfIdentifier;
							}

							final String rdfType = contentItem.get(ResponseKey.RDF_TYPE)
									.getAsString();
							
							switch (rdfType) {

							
							case "concept": {
								countConcept++;
								final IRI contextItemId = vf.createIRI(rdfIdentifier);

								model.add(contextItemId, RDF.TYPE, SKOS.CONCEPT, PRIMAL.CONCEPTS);
								
								ResponseValues.processStringValue(contextItemId, SKOS.PREF_LABEL, model,
										PRIMAL.CONCEPTS, contentItem, ResponseKey.SKOS_PREF_LABEL);

								final JsonArray altLabels = contentItem.get(ResponseKey.SKOS_ALT_LABEL)
										.getAsJsonArray();
								for (int j = 0; j < altLabels.size(); j++) {
									final String altLabel = altLabels.get(j).getAsString();
									model.add(contextItemId, SKOS.ALT_LABEL, vf.createLiteral(altLabel), PRIMAL.CONCEPTS);
								}

								break;
							}
							
							
							case "content": {
								countContent++;
								// using full Primal URL rather than target website address
								String url = contentItem.get(ResponseKey.SCHEMA_URL).getAsString();
								final IRI contentItemId = vf.createIRI(url);

								model.add(contentItemId, DC.RELATION, vf.createLiteral(rdfIdentifier), PRIMAL.CONTENT);

								model.add(contentItemId, RDF.TYPE, PRIMAL.CONTENT_ITEM, PRIMAL.CONTENT);

								model.add(contentItemId, DC.IDENTIFIER, vf.createLiteral(rdfIdentifier),
										PRIMAL.CONTENT);

								ResponseValues.processStringValue(contentItemId, DC.TITLE, model, PRIMAL.CONTENT,
										contentItem, ResponseKey.SCHEMA_NAME);

								ResponseValues.processStringValue(contentItemId, DC.DESCRIPTION, model,
										PRIMAL.CONTENT, contentItem, ResponseKey.SCHEMA_DESCRIPTION);

								ResponseValues.processStringValue(contentItemId, DC.PUBLISHER, model,
										PRIMAL.CONTENT, contentItem, ResponseKey.SCHEMA_PUBLISHER);

								// TODO foaf:img possibly isn't best option, eg schema:image ?
								ResponseValues.processStringValue(contentItemId, FOAF.IMG, model,
										PRIMAL.CONTENT, contentItem, ResponseKey.SCHEMA_IMAGE);

								ResponseValues.processStringValue(contentItemId, DC.SOURCE, model, PRIMAL.CONTENT,
										contentItem, ResponseKey.SCHEMA_PROVIDER);

								ResponseValues.processDateValue(contentItemId, DC.DATE, model, PRIMAL.CONTENT,
										contentItem, ResponseKey.SCHEMA_DATE_PUBLISHED);

								ResponseValues.processFloatValue(contentItemId, PRIMAL.CONTENT_SCORE, model,
										PRIMAL.CONTENT, contentItem, ResponseKey.PRIMAL_CONTENT_SCORE);

								final JsonArray subjects = contentItem.get(ResponseKey.SCHEMA_KEYWORDS)
										.getAsJsonArray();
								for (int j = 0; j < subjects.size(); j++) {
									final String subject = subjects.get(j).getAsString();
									model.add(contentItemId, DC.SUBJECT, vf.createIRI(PRIMAL.TOPIC_NAMESPACE, subject), PRIMAL.CONTENT);
								}
								break;
							}
							
							
							default:
								countUnknown++;
								logger.warn("unknown Primal type: '{}' for item {}", rdfType, rdfIdentifier);
								continue;
							}
						}

						logger.debug("response contains {} concept and {} content items ({} unknown)", countConcept, countContent, countUnknown);

					} else {
						// TODO can this happen?
						logger.warn("json element " + ResponseKey.RDF_GRAPH
								+ " of unexpected type. Expected array.");
					}
					logger.debug("response processing complete");
				} else {
					logger.warn("Unexpected response {} ", httpCode);
					logResponseDetails(response);
					if (HTTP_UNAUTHORIZED == httpCode) {
						throw new PrimalAuthenticationException();
					} else {
						throw new PrimalException("error retrieving results.", httpCode);
					}
				}
			} finally {
				EntityUtils.consumeQuietly(response.getEntity());
			}
		} catch (final IOException e) {
			logger.warn("I/O error retrieving result", e);
			throw new PrimalException("I/O error retrieving result", 0, e);
		}

		return model;
	}

	public void addConcept(String topic, boolean expand) throws PrimalException {
		addConcept(topic, null, expand);
	}

	/**
	 * Creates a topic.
	 * 
	 * @param topic
	 *            Specification of the core topic of interest for this add request. Values should be provided
	 *            in a forward slash and/or semi-colon delimited format, where slashes delimit a conceptual
	 *            hierarchy, and semi-colons provide a breadth of conceptual detail at the same hierarchical
	 *            level. For example:
	 *            <ul>
	 *            <li><code>tidal+research</code><br>
	 *            Implies a simple use case of interest in the field of tidal research.</li>
	 *            <li><code>baking/cookies</code><br>
	 *            Implies interest in cookies of the edible variety rather than the browser data storage
	 *            variety.</li>
	 *            <li><code>shelter/caves;houses;tents</code><br>
	 *            Implies interest in the concepts of caves, houses and tents as being types of shelters.</li>
	 *            </ul>
	 * @param contentSource
	 *            source of content. If set to null, the default content source(s) will be used.
	 * @param expand
	 *            Indicates if Primal should initiate semantic expansion on the submitted concept.
	 * 
	 * @throws PrimalException
	 *             if an error occurred while adding the concept.
	 */
	public void addConcept(String topic, String contentSource, boolean expand) throws PrimalException {
		final java.net.URI requestURL = createRequestURI(contentSource, 0, 0, 0);

		HttpUriRequest method = null;
		if (expand) {
			method = new HttpPost(requestURL);

		} else {
			method = new HttpPut(requestURL);
		}
		try {
			configureRequest(method, topic);
			final HttpCoreContext context = new HttpCoreContext();
			final HttpResponse response = httpClient.execute(method, context);
			logRequestDetails(method, context);
			try {
				final int httpCode = response.getStatusLine().getStatusCode();
				logger.debug("http response code:" + httpCode);
				if (HTTP_CREATED != httpCode) {
					logger.warn("Unexpected response {} ", httpCode);
					logResponseDetails(response);
					if (HTTP_UNAUTHORIZED == httpCode) {
						throw new PrimalAuthenticationException();
					} else {
						throw new PrimalException("error executing add request", httpCode);
					}
				}
			} finally {
				EntityUtils.consumeQuietly(response.getEntity());
			}
		} catch (final IOException e) {
			logger.warn("error executing add request", e);
			throw new PrimalException("error executing add request", 0, e);
		}
	}

	public void deleteConcept(String topicOfInterest) throws PrimalException {
		deleteConcept(topicOfInterest, null);
	}

	/**
	 * Removes a concept from an interest network.
	 * 
	 * @param topic
	 *            Specification of the core topic of interest for this delete request. Values should be
	 *            provided in a forward slash and/or semi-colon delimited format, where slashes delimit a
	 *            conceptual hierarchy, and semi-colons provide a breadth of conceptual detail at the same
	 *            hierarchical level. For example:
	 *            <ul>
	 *            <li><code>tidal+research</code><br>
	 *            Implies a simple use case of interest in the field of tidal research.</li>
	 *            <li><code>baking/cookies</code><br>
	 *            Implies interest in cookies of the edible variety rather than the browser data storage
	 *            variety.</li>
	 *            <li><code>shelter/caves;houses;tents</code><br>
	 *            Implies interest in the concepts of caves, houses and tents as being types of shelters.</li>
	 *            </ul>
	 * @param contentSource
	 *            source of content.
	 * 
	 * @throws PrimalException
	 *             if an error occurred while removing the concept.
	 */
	public void deleteConcept(String topic, String contentSource) throws PrimalException {
		final java.net.URI requestURL = createRequestURI(contentSource, 0, 0, 0);

		HttpDelete method = new HttpDelete(requestURL);
		try {
			configureRequest(method, topic);
			final HttpCoreContext context = new HttpCoreContext();
			final HttpResponse response = httpClient.execute(method, context);
			logRequestDetails(method, context);
			try {
				final int httpCode = response.getStatusLine().getStatusCode();
				logger.debug("http response code:" + httpCode);
				if (HTTP_OK != httpCode) {
					logger.warn("Unexpected response {} ", httpCode);
					logResponseDetails(response);
					if (HTTP_UNAUTHORIZED == httpCode) {
						throw new PrimalAuthenticationException();
					} else {
						throw new PrimalException("error executing delete request.", httpCode);
					}
				}
			} finally {
				EntityUtils.consumeQuietly(response.getEntity());
			}
		} catch (final IOException e) {
			logger.warn("error executing delete request", e);
			throw new PrimalException("error executing delete request", 0, e);
		}
	}

	/**
	 * Shut down this client, freeing up resources.
	 */
	@Deprecated
	public void shutdown() {
	}

	/* protected/private methods */

	/**
	 * Adds required API headers and inserts topic into body. Assumes topic path is URI encoded.
	 */
	private final void configureRequest(HttpRequest request, String topic) throws UnsupportedEncodingException {
		if (primalAccount.getApplicationID() != null) {
			request.addHeader(PRIMAL_API_HEADER_APP_ID, primalAccount.getApplicationID());
		}
		if (primalAccount.getApplicationKey() != null) {
			request.addHeader(PRIMAL_API_HEADER_APP_KEY, primalAccount.getApplicationKey());
		}

		// Primal requires pre-emptive Basic authentication
		if (this.accountCredentials != null) {
			try {
				request.setHeader(new BasicScheme().authenticate(this.accountCredentials, request, null));
			} catch (AuthenticationException e) {
				// shouldn't happen.
				throw new RuntimeException(e);
			}
		}
		
		if (topic != null) {
			final String body = createQueryBody(topic);
			logger.debug("primal request body: {}", body);
			HttpEntity entity = new StringEntity(body);	
			if (request instanceof HttpPost) {
				((HttpPost) request).setEntity(entity);
				request.addHeader("Content-type", "application/json");
			}
			if (request instanceof HttpPut) {
				((HttpPut) request).setEntity(entity);
				request.addHeader("Content-type", "application/json");
			}
		}
	}

	protected String createQueryBody(String topicPath) {
		// topic path should be encoded, but we'll replace quotes just in case, so the JSON doesn't break
		topicPath = topicPath.replaceAll("\"", "\\\\\"");
		return "{\"uris\":[\"" + topicPath + "\"]}";
	}

	private java.net.URI createRequestURI(String contentSource, float minScore,
			int maxContentCount, int wait) {
		URIBuilder builder = new URIBuilder().setScheme(primalApi.getProtocol()).setHost(primalApi.getHost())
				.setPath(primalApi.getPath());

		if (subAccount != null) {
			builder.addParameter(PRIMAL_API_PARAM_USERNAME, subAccount);
		}
		builder.addParameter(PRIMAL_API_PARAM_TYPES, PRIMAL_API_DEFAULT_TYPES);
		if (minScore > 0) {
			builder.addParameter(PRIMAL_API_PARAM_MIN_CONTENT_SCORE, Float.toString(minScore));
		}
		if (maxContentCount > 0) {
			builder.addParameter(PRIMAL_API_PARAM_MAX_CONTENT_ITEMS, Integer.toString(maxContentCount));
		}
		if (contentSource != null) {
			builder.addParameter(PRIMAL_API_PARAM_CONTENT_SOURCES, contentSource);
		}
		// TODO will we re-instate wait time manually?

		java.net.URI uri;
		try {
			uri = builder.build();
		} catch (URISyntaxException e) {
			throw new RuntimeException(e);
		}
		return uri;
	}

	private void logRequestDetails(HttpRequest method, HttpCoreContext context) {
		if (method == null) {
			return;
		}
		logger.debug("{} request on {} ", method.getClass().getSimpleName(),
				method.getRequestLine().getUri());
		for (Header header : method.getAllHeaders()) {
			logger.trace("\t{}: {}", header.getName(), header.getValue());
		}
	}

	private void logResponseDetails(HttpResponse response) {
		logger.debug("Response HTTP {} : {}", response.getStatusLine().getStatusCode(),
				response.getStatusLine().getReasonPhrase());
		for (Header header : response.getAllHeaders()) {
			logger.debug("\t{}: {}", header.getName(), header.getValue());
		}
		try {
			logger.debug("\tresponse body: ", IOUtils.toString(response.getEntity().getContent()));
		} catch (Exception e) {
			// fall through
		}
	}
}
