package com.insightng.thirdparty.primal;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonNull;
import com.google.gson.JsonObject;

public class ResponseValues {

	private static final Logger logger = LoggerFactory.getLogger(ResponseValues.class);

	private static final ValueFactory vf = SimpleValueFactory.getInstance();

	private ResponseValues() {
		// static util class
	}
	
	/**
	 * Retrieves the value identified by the supplied key from the json object
	 * and processes it as an integer value. The resulting value is added to the
	 * supplied {@link Model} as an integer literal value using the supplied
	 * subject and property.
	 * 
	 * @param subject
	 * @param property
	 * @param model
	 * @param context
	 * @param jsonObject
	 * @param key
	 * @return the Integer value.
	 */
	public static Integer processIntValue(Resource subject, IRI property, Model model, Resource context,
			JsonObject jsonObject, String key) {
		Integer value = null;
		if (jsonObject.has(key) && !(jsonObject.get(key) instanceof JsonNull)) {
			value = jsonObject.get(key).getAsInt();
			model.add(subject, property, vf.createLiteral(value), context);
		}
		return value;
	}

	/**
	 * Retrieves the value identified by the supplied key from the json object
	 * and processes it as an float value. The resulting value is added to the
	 * supplied {@link Model} as a float literal value using the supplied
	 * subject and property.
	 * 
	 * @param subject
	 * @param property
	 * @param model
	 * @param context
	 * @param jsonObject
	 * @param key
	 * @return the float value.
	 */
	public static Float processFloatValue(Resource subject, IRI property, Model model, Resource context,
			JsonObject jsonObject, String key) {
		Float value = null;
		if (jsonObject.has(key) && !(jsonObject.get(key) instanceof JsonNull)) {
			value = jsonObject.get(key).getAsFloat();
			model.add(subject, property, vf.createLiteral(value), context);
		}
		return value;
	}

	/**
	 * Retrieves the value identified by the supplied key from the json object
	 * and processes it as a boolean value. The resulting value is added to the
	 * supplied {@link Model} as a boolean literal value using the supplied
	 * subject and property.
	 * 
	 * @param subject
	 * @param property
	 * @param model
	 * @param context
	 * @param jsonObject
	 * @param key
	 * @return the boolean value.
	 */
	public static Boolean processBooleanValue(Resource subject, IRI property, Model model, Resource context,
			JsonObject jsonObject, String key) {
		Boolean value = null;
		if (jsonObject.has(key) && !(jsonObject.get(key) instanceof JsonNull)) {
			value = jsonObject.get(key).getAsBoolean();
			model.add(subject, property, vf.createLiteral(value), context);
		}
		return value;
	}

	/**
	 * Retrieves the value identified by the supplied key from the json object
	 * and processes it as a Date value. The resulting value is added to the
	 * supplied {@link Model} as a XML Datetime literal value using the supplied
	 * subject and property.
	 * 
	 * @param subject
	 * @param property
	 * @param model
	 * @param context
	 * @param jsonObject
	 * @param key
	 * @return the Date value, or <code>null</code> if the value could not be
	 *         parsed into a legal Date value.
	 */
	public static Date processDateValue(Resource subject, IRI property, Model model, Resource context,
			JsonObject jsonObject, String key) {
		Date value = null;

		SimpleDateFormat primalDateFormat = new SimpleDateFormat("MMMM dd, yyyy hh:mm:ss a Z");
		if (jsonObject.has(key) && !(jsonObject.get(key) instanceof JsonNull)) {
			String dateString = jsonObject.get(key).getAsString();
			// fix timezone
			if (dateString.endsWith(":00")) {
				dateString = dateString.replaceAll(":00$", "00");
			}
			try {
				value = primalDateFormat.parse(dateString);
			} catch (ParseException e) {
				logger.warn("could not parse date value '{}'", dateString, e);
				return null;
			}
			model.add(subject, property, vf.createLiteral(value), context);
		}
		return value;
	}

	/**
	 * Retrieves the value identified by the supplied key from the json object
	 * and processes it as a string value. The resulting value is added to the
	 * supplied {@link Model} as a plain literal value using the supplied
	 * subject and property.
	 * 
	 * @param subject
	 * @param property
	 * @param model
	 * @param context
	 * @param jsonObject
	 * @param key
	 * @return the string value.
	 */
	public static String processStringValue(Resource subject, IRI property, Model model, Resource context,
			JsonObject jsonObject, String key) {
		String value = null;
		if (jsonObject.has(key) && !(jsonObject.get(key) instanceof JsonNull)) {
			value = jsonObject.get(key).getAsString();
			model.add(subject, property, vf.createLiteral(value), context);
		}
		return value;
	}

}
