package com.insightng.thirdparty.primal.exception;

import static java.net.HttpURLConnection.HTTP_UNAUTHORIZED;

public class PrimalAuthenticationException extends PrimalException {

	private static final long serialVersionUID = -7895477201685706348L;

	public static final String MESSAGE = "authentication failed";

	public PrimalAuthenticationException() {
		super(MESSAGE, HTTP_UNAUTHORIZED);
	}
}
