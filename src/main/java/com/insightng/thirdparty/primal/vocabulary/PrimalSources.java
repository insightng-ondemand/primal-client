package com.insightng.thirdparty.primal.vocabulary;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;

/**
 * Constants for the default Primal content sources.
 * 
 * @author Jeen Broekstra
 * 
 */
public class PrimalSources {

	private static final String namespace = PRIMAL.NAMESPACE + "@PrimalSources/";

	public static final IRI NEWS = create("News");
	public static final IRI BOOKS = create("Books");
	public static final IRI REFERENCE = create("Reference");
	public static final IRI SOCIAL = create("Social");
	public static final IRI VIDEOS = create("Videos");
	public static final IRI WEB = create("Web");
	public static final IRI IMAGES = create("Images");

	private static final IRI create(String sourceName) {
		return SimpleValueFactory.getInstance().createIRI(namespace, sourceName);
	}

	private PrimalSources() {
		// static class
	}
}
