/* Copyright InsightNG (http://www.insightng.com/) 2012-2013
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   - Neither the name of InsightNG nor the names of its contributors may be used 
 *     to endorse or promote products derived from this software without specific 
 *     prior written permission.
 *     
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 *  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.insightng.thirdparty.primal.vocabulary;

/**
 * String constants for the keys used in the Primal JSON response.
 * 
 * @author Jeen Broekstra
 * 
 */
public class ResponseKey {

	/* primal response json keys for both concepts and content */

	public static final String RDF_GRAPH = "@graph";
	public static final String RDF_IDENTIFIER = "@id";	
	public static final String RDF_TYPE = "@type";

	/* primal response json keys for (skos) concepts */

	public static final String SKOS_PREF_LABEL = "label";
	public static final String SKOS_ALT_LABEL = "alternateLabels";

	/* primal response json keys for content */
	
	public static final String SCHEMA_URL = "url";
	public static final String SCHEMA_NAME = "title";
	public static final String SCHEMA_ADDITIONAL_TYPE = "contentType";
	public static final String SCHEMA_PROVIDER = "provider";
	public static final String SCHEMA_KEYWORDS = "keywords";
	public static final String SCHEMA_PUBLISHER = "publisher";
	public static final String SCHEMA_DESCRIPTION = "description";
	public static final String SCHEMA_DATE_PUBLISHED = "datePublished";
	public static final String SCHEMA_IMAGE = "image";
	public static final String PROV_GENERATED_AT_TIME = "generatedAt";
	public static final String PRIMAL_CONTENT_SCORE = "contentScore";

	/* primal response json keys for response info */

	public static final String PRIMAL_RESPONSE_INFO = "responseInfo";
	/** The number of terms that comprise the user-context of the input. A higher number of context terms generates higher quality results. If null, then a user-context was not used to generate the results. */
	public static final String PRIMAL_UNIQUE_TERM_COUNT = "uniqueTermCount";
	/** The number of unique terms extracted from the input. A higher number of unique terms generates higher quality results. */
	public static final String PRIMAL_CONTEXT_TERM_COUNT = "contextTermCount";
	/** Indicates whether additional text was used to enhance a textually sparse input. When this occurs, the input provided to Primal was sparse and we have less confidence in the relevance of the results. */
	public static final String PRIMAL_SUBSTITUTE_USED = "substituteUsed";
}
