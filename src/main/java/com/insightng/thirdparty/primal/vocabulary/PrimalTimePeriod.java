package com.insightng.thirdparty.primal.vocabulary;

public enum PrimalTimePeriod {
	TwentyFourHours("24hours"), OneWeek("1week"), OneMonth("1month");

	private final String name;

	private PrimalTimePeriod(String name) {
		this.name = name;
	}

	public String toString() {
		return name;
	}
}
