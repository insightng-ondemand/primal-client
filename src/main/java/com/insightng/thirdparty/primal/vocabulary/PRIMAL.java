/* Copyright InsightNG (http://www.insightng.com/) 2012-2013
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   - Neither the name of InsightNG nor the names of its contributors may be used 
 *     to endorse or promote products derived from this software without specific 
 *     prior written permission.
 *     
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 *  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.insightng.thirdparty.primal.vocabulary;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;

/**
 * Constants for RDF representation of Primal response data.
 * 
 * @author Jeen Broekstra
 * 
 */
public class PRIMAL {
	public static final String NAMESPACE = "https://data.primal.com/";
	public static final String TOPIC_NAMESPACE = "https://data.primal.com/topic/";

	public static final String PREFIX = "primal";

	/**
	 * Context for Primal response info
	 */
	public static final IRI RESPONSE_INFO;

	/**
	 * Context for Primal concepts
	 */
	public static final IRI CONCEPTS;

	/**
	 * Context for Primal content items
	 */
	public static final IRI CONTENT;

	/**
	 * primal:conceptScore property.
	 */
	public static final IRI CONCEPT_SCORE;

	/**
	 * primal:ContentItem, the class of content items (web documents, etc.).
	 */
	public static final IRI CONTENT_ITEM;

	/**
	 * primal:contentScore property.
	 */
	public static final IRI CONTENT_SCORE;

	/** primal:contentCount property. */
	public static final IRI UNIQUE_TERM_COUNT;

	/** primal:totalConceptsCount property */
	public static final IRI TOTAL_CONTEXT_TERM_COUNT;

	/** primal:conceptCount property */
	public static final IRI PRIMAL_SUBSTITUTE_USED;

    /** primal:highContentScore property. */
    public static final IRI HIGH_CONTENT_SCORE;

	static {
		final ValueFactory f = SimpleValueFactory.getInstance();

		RESPONSE_INFO = f.createIRI(NAMESPACE, "ResponseInfo");
		CONCEPTS = f.createIRI(NAMESPACE, "Concepts");
		CONTENT = f.createIRI(NAMESPACE, "Content");
		CONTENT_ITEM = f.createIRI(NAMESPACE, "ContentItem");
		CONCEPT_SCORE = f.createIRI(NAMESPACE, "conceptScore");
		CONTENT_SCORE = f.createIRI(NAMESPACE, "contentScore");
		UNIQUE_TERM_COUNT = f.createIRI(NAMESPACE, "uniqueTermCount");
		TOTAL_CONTEXT_TERM_COUNT = f.createIRI(NAMESPACE, "contextTermCount");
		PRIMAL_SUBSTITUTE_USED = f.createIRI(NAMESPACE, "conceptCount");
        HIGH_CONTENT_SCORE = f.createIRI(NAMESPACE, "highContentScore");
	}
}
